/* VARIÁVEIS
=============================================*/

//Link Botões: controle de tamanho
var btnAumentarLetra = document.getElementById("btnAumentarLetra");
var btnDiminuirLetra = document.getElementById("btnDiminuirLetra");
var btnTamanhoNormal = document.getElementById("btnTamanhoNormal");
var btnTrocarFonte = document.getElementById("bt_contraste");

//Tamanho inicial:
var tamanho = 11;

//Tamanho salvo em cookie
var tamanhoCookie = getCookie("tamanhoFonte");
//Contraste salvo em cookie
var altoContraste = getCookie("AltoContraste");

/* FUNÇÕES
=============================================*/

//Cálculo de alteração de tamanho.
function alterarTamanho(sinal) {

	if (sinal == "mais" && tamanho < 20) {
		tamanho += 1;

	} else  if (sinal == "menos" && tamanho > 9) {
		tamanho -= 1;

	} else if (sinal == "igual") {
		tamanho = 11;

	}

	if (document.getElementById("saiba-mais") != null) {
		document.getElementById("saiba-mais").style.top = (428 + (tamanho-11) *18) + "px";
		document.getElementById("video").style.top = (428 + (tamanho-11) *18) + "px";
	}

	mudarTamanho(tamanho);
}


//Mudar Tamanho
function mudarTamanho(tam){
	document.getElementById("pre_conteudo").style.fontSize = tam + "px";

	setCookie("tamanhoFonte", tam);
}


//----------------------
// - FUNÇÕES DE COOKIE -

// Salvar cookie
function setCookie(nome, valor){
    var hoje = new Date();
    var expira = new Date(hoje.getTime() + 999 * 24 * 60 * 60 * 1000);
    var expira = expira.toGMTString();
       
    document.cookie = nome + "=" + valor + ";expires=" + expira + ";path=/";
    
}

// Ler cookie.
function getCookie(strCookie){
    var strNomeIgual = strCookie + "=";
    var arrCookies = document.cookie.split(';');
    for (var i = 0; i < arrCookies.length; i++) {
        var strValorCookie = arrCookies[i];
        while (strValorCookie.charAt(0) == ' ') {
            strValorCookie = strValorCookie.substring(1, strValorCookie.length);
        }
        if (strValorCookie.indexOf(strNomeIgual) == 0) {
            return strValorCookie.substring(strNomeIgual.length, strValorCookie.length);
        }
    }
    return '';
}

// Excluir cookie.
function unsetCookie(nome){
    document.cookie = nome + "='';expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

// - FIM DE FUNÇÕES DE COOKIE -
//-----------------------------



function mudarContraste() {

	if(document.body.className == "acessibilidade") {
		document.body.className = "";
	} else {
		document.body.className = "acessibilidade";
	}
	setCookie("AltoContraste", document.body.className);
}


/* AÇÕES
=============================================*/

//Ações a serem feitas ao início do carregamento da página
function onLoad() {

	//Verificar se existe tamanho salvo
	if (tamanhoCookie != ""){

		//Mudar o tamanho atual
		mudarTamanho(tamanhoCookie);

		//Definir o tamanho
		tamanho = parseInt(tamanhoCookie);
		if (document.getElementById("saiba-mais") != null || document.getElementById("video") != null) {
			document.getElementById("saiba-mais").style.top = (428 + (tamanho-11) *18) + "px";
			document.getElementById("video").style.top = (428 + (tamanho-11) *18) + "px";
		}
	} else {
		mudarTamanho(tamanho);
	}
	if (altoContraste == "acessibilidade") {

		//Mudar o tamanho atual
		mudarContraste();
	}
}