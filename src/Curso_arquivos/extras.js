/* CLIPAGEM VIRTUAL
=============================================*/

function mostrar_artigosClipagem(idconteudo, id, extra) {

	//Pegar o valor da classe
	var analizar_escondido = document.getElementById("artigos-clipagem_virtual-conteudo" + idconteudo);
	var mais_menos = document.getElementById("artigos-clipagem_virtual-MosEsc" + id);

	//Analizando e alterando

	//Se esconder -> mostrar
	if (analizar_escondido.className == "artigos-clipagem_virtual-escondido") {
		analizar_escondido.className = "artigos-clipagem_virtual-Nescondido";
		if (extra == "sim") {
			mais_menos.innerHTML = "menos -";
		}

	//Se mostrar -> esconder
	} else {
		analizar_escondido.className = "artigos-clipagem_virtual-escondido";
		if (extra == "sim") {
			mais_menos.innerHTML = "mais +"
		}
	}

}